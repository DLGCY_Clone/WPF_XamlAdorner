﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Threading;

namespace AdornedControlSample
{
	/// <summary>
	/// Interaction logic for Window1.xaml
	/// </summary>
	public partial class Window1 : Window
    {
        private static readonly double fadeOutTime = 1;
        private static readonly double fadeInTime = 0.25;

        public Window1()
        {
            InitializeComponent();

            closeButtonFadeoutTimer.Tick += CloseButtonFadeoutTimer_Tick;
            closeButtonFadeoutTimer.Interval = TimeSpan.FromSeconds(2);
        }

        enum State
        {
            Hidden,
            Visible,
        }

        private void Ellipse_MouseEnter(object sender, MouseEventArgs e)
        {
            bool wasFadingOut = _fadeOutAnimation != null;
            _fadeOutAnimation = null; // Abort fade out.

            if (closeButtonFadeoutTimer.IsEnabled)
            {
                Trace.WriteLine("Ellipse_MouseEnter: Stopped fade out delay timer.");

                closeButtonFadeoutTimer.Stop();
            }

            if (!adornedControl.IsAdornerVisible)
            {
                Trace.WriteLine("Ellipse_MouseEnter: Adorner hidden, fading it in.");

                adornedControl.ShowAdorner();

                DoubleAnimation doubleAnimation2 = new DoubleAnimation(1.0, new Duration(TimeSpan.FromSeconds(fadeInTime)));
                doubleAnimation2.Completed += DoubleAnimation2_Completed;
                adornerCanvas.BeginAnimation(Canvas.OpacityProperty, doubleAnimation2);
            }
            else if (wasFadingOut)
            {
                // Was fading out, fade back in.
                Trace.WriteLine("CloseButton_MouseEnter: Was fading out, fade back in.");

                DoubleAnimation doubleAnimation = new DoubleAnimation(1.0, new Duration(TimeSpan.FromSeconds(fadeInTime)));
                adornerCanvas.BeginAnimation(Canvas.OpacityProperty, doubleAnimation);
            }
            else
            {
                adornerCanvas.BeginAnimation(Canvas.OpacityProperty, null);
            }
        }

        void DoubleAnimation2_Completed(object sender, EventArgs e)
        {
            Trace.WriteLine("DoubleAnimation2_Completed: Finished adorner fade in.");
        }

        private void Ellipse_MouseLeave(object sender, MouseEventArgs e)
        {
            Trace.WriteLine("Ellipse_MouseLeave: Started fade out delay timer.");

            closeButtonFadeoutTimer.Start();
        }

        DoubleAnimation _fadeOutAnimation = null;

        void CloseButtonFadeoutTimer_Tick(object sender, EventArgs e)
        {
            Trace.WriteLine("CloseButtonFadeoutTimer_Tick: Fade out delay timer elapsed, starting fade out.");

            closeButtonFadeoutTimer.Stop();

            _fadeOutAnimation = new DoubleAnimation(0.0, new Duration(TimeSpan.FromSeconds(fadeOutTime)));
            _fadeOutAnimation.Completed += DoubleAnimation_Completed;
            adornerCanvas.BeginAnimation(Canvas.OpacityProperty, _fadeOutAnimation);
        }

        void DoubleAnimation_Completed(object sender, EventArgs e)
        {
            if (_fadeOutAnimation == null)
            {
                Trace.WriteLine("DoubleAnimation_Completed: Fade out aborted.");
            }
            else
            {
                Trace.WriteLine("DoubleAnimation_Completed: Fade out complete, hiding the adorner.");

                adornedControl.HideAdorner();

                _fadeOutAnimation = null;
            }
        }

        DispatcherTimer closeButtonFadeoutTimer = new DispatcherTimer();

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            canvas.Children.Remove(adornedControl);
        }

        private void CloseButton_MouseEnter(object sender, MouseEventArgs e)
        {
            bool wasFadingOut = _fadeOutAnimation != null;
            _fadeOutAnimation = null; // Abort fade out.

            if (closeButtonFadeoutTimer.IsEnabled)
            {
                Trace.WriteLine("CloseButton_MouseEnter: Fade out delay timer active, stopping it.");

                closeButtonFadeoutTimer.Stop();
            }

            Trace.Assert(adornedControl.IsAdornerVisible);

            if (!adornedControl.IsAdornerVisible)
            {
                Trace.WriteLine("CloseButton_MouseEnter: Adorner is hidden, showing it!");

                adornedControl.ShowAdorner();

                DoubleAnimation doubleAnimation = new DoubleAnimation(1.0, new Duration(TimeSpan.FromSeconds(fadeInTime)));
                adornerCanvas.BeginAnimation(Canvas.OpacityProperty, doubleAnimation);
            }
            else if (wasFadingOut)
            {
                // Was fading out, fade back in.
                Trace.WriteLine("CloseButton_MouseEnter: Was fading out, fade back in.");

                DoubleAnimation doubleAnimation = new DoubleAnimation(1.0, new Duration(TimeSpan.FromSeconds(fadeInTime)));
                adornerCanvas.BeginAnimation(Canvas.OpacityProperty, doubleAnimation);
            }
            else
            {
                Trace.WriteLine("CloseButton_MouseEnter: Adorner is not hidden, clearing animation!");

                adornerCanvas.BeginAnimation(Canvas.OpacityProperty, null);
            }
        }

        private void CloseButton_MouseLeave(object sender, MouseEventArgs e)
        {
            Trace.WriteLine("CloseButton_MouseLeave: Started fade out delay timer.");

            closeButtonFadeoutTimer.Start();
        }

        private void Thumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            Canvas.SetLeft(adornedControl, Canvas.GetLeft(adornedControl) + e.HorizontalChange);
            Canvas.SetTop(adornedControl, Canvas.GetTop(adornedControl) + e.VerticalChange);
        }
    }
}
