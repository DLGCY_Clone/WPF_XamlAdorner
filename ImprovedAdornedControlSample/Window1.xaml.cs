﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace AdornedControlSample
{
	/// <summary>
	/// Interaction logic for Window1.xaml
	/// </summary>
	public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            canvas.Children.Remove(adornedControl);
        }

        private void Thumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            Canvas.SetLeft(adornedControl, Canvas.GetLeft(adornedControl) + e.HorizontalChange);
            Canvas.SetTop(adornedControl, Canvas.GetTop(adornedControl) + e.VerticalChange);
        }
    }
}
